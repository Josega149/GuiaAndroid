package com.piazuelo.domiciliosmoviles.activities;


import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;

import com.piazuelo.domiciliosmoviles.R;

import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Before;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.scrollTo;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withParent;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class PedidoActivityTest {

    @Rule
    public ActivityTestRule<MainActivity> mActivityTestRule = new ActivityTestRule<>(MainActivity.class);

    @Before
    public void init()  throws InterruptedException {
        Thread.sleep(1000);
        ViewInteraction relativeLayout = onView(
                allOf(childAtPosition(
                        allOf(withId(R.id.platos_listview),
                                withParent(withId(R.id.activity_main))),
                        3),
                        isDisplayed()));
        relativeLayout.perform(click());
    }

    @Test
    public void titlePedidoTest() {
        onView(allOf(withParent(withId(R.id.toolbar)), withText("Combo hamburguesa")))
                .check(matches(isDisplayed()));
    }

    @Test
    public void metodoDePagoTest() {
        ViewInteraction metodoButton = onView(allOf(withId(R.id.payment_method)));
        metodoButton.perform(scrollTo(), click());

        ViewInteraction radioButton = onView(
                allOf(withId(R.id.credito_btn),
                        withText("Tarjeta de crédito"))
        );

        radioButton.perform(click());

        ViewInteraction selectButton = onView(
                allOf(
                        withId(R.id.select_btn),
                        withText("Seleccionar"),
                        isDisplayed()
                )
        );

        selectButton.perform(click());

        metodoButton.check(matches(withText("Tarjeta de crédito")));
    }

    @Test
    public void realizarPedidoTest() throws InterruptedException {
        ViewInteraction nombreText = onView(withId(R.id.client_name));
        nombreText.perform(scrollTo(), replaceText("Mario Linares"), closeSoftKeyboard());

        ViewInteraction lugarText = onView(withId(R.id.order_place));
        lugarText.perform(scrollTo(), replaceText("ML 652"), closeSoftKeyboard());

        ViewInteraction metodoButton = onView(allOf(withId(R.id.payment_method)));
        metodoButton.perform(scrollTo(), click());

        ViewInteraction radioButton = onView(allOf(withId(R.id.credito_btn)));
        radioButton.perform(click());

        ViewInteraction selectButton = onView(allOf(withId(R.id.select_btn)));
        selectButton.perform(click());

        ViewInteraction propinaButton = onView(allOf(withId(R.id.mas)));
        propinaButton.perform(scrollTo(), click());

        ViewInteraction pedirButton = onView(allOf(withId(R.id.pedir_btn)));
        pedirButton.perform(scrollTo(), click());

        Thread.sleep(1500);

        onView(allOf(withId(android.support.design.R.id.snackbar_text), withText("Su pedido fue registrado"))).check(matches(isDisplayed()));
    }

    @Test
    public void pedidoActivityTest() throws InterruptedException {
        ViewInteraction appCompatButton = onView(
                allOf(withId(R.id.mas), withText("+1000")));
        appCompatButton.perform(scrollTo(), click());

        ViewInteraction textView = onView(
                allOf(withId(R.id.propina_text), withText("$2000"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.fragment_propina),
                                        0),
                                1),
                        isDisplayed()));
        textView.check(matches(withText("$2000")));

        ViewInteraction appCompatButton2 = onView(
                allOf(withId(R.id.menos), withText("-1000")));
        appCompatButton2.perform(scrollTo(), click());

        ViewInteraction textView2 = onView(
                allOf(withId(R.id.propina_text), withText("$1000"),
                        childAtPosition(
                                childAtPosition(
                                        withId(R.id.fragment_propina),
                                        0),
                                1),
                        isDisplayed()));
        textView2.check(matches(withText("$1000")));

    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
