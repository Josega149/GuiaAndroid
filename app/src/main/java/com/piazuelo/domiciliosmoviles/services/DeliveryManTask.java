package com.piazuelo.domiciliosmoviles.services;

import android.content.AsyncTaskLoader;
import android.content.Context;

/**
 * Created by jgtamura on 10/17/17.
 */

public class DeliveryManTask extends AsyncTaskLoader {
    public DeliveryManTask(Context context) {
        super(context);
    }

    @Override
    public String loadInBackground() {
        try {
            Thread.sleep(3000);
            return "Domiciliario de Prueba";
        } catch (InterruptedException e) {
            e.printStackTrace();
            return "Error";
        }
    }
}