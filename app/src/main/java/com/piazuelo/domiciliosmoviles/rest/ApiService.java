package com.piazuelo.domiciliosmoviles.rest;

/**
 * Created by jgtamura on 10/17/17.
 */


        import com.piazuelo.domiciliosmoviles.entities.Pedido;
        import com.piazuelo.domiciliosmoviles.entities.Plato;
        import com.piazuelo.domiciliosmoviles.entities.ResponseMessage;

        import java.util.List;

        import retrofit2.Call;
        import retrofit2.http.Body;
        import retrofit2.http.GET;
        import retrofit2.http.POST;

public interface ApiService {

    @GET("/platos")
    Call<List<Plato>> getPlatos();

    @POST("/pedidos")
    Call<ResponseMessage> createPedido(@Body Pedido pedido);

}