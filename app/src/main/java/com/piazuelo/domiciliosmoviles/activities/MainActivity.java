package com.piazuelo.domiciliosmoviles.activities;

import android.app.ProgressDialog;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.AsyncTask;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.GsonBuilder;
import com.loopj.android.http.JsonHttpResponseHandler;
import com.piazuelo.domiciliosmoviles.R;
import com.piazuelo.domiciliosmoviles.adapters.PlatoAdapter;
import com.piazuelo.domiciliosmoviles.entities.Plato;
import com.piazuelo.domiciliosmoviles.rest.RestClient;
import com.piazuelo.domiciliosmoviles.services.GpsIntentService;

import org.json.JSONArray;
import org.json.JSONException;

import java.util.ArrayList;
import java.util.List;

import cz.msebera.android.httpclient.Header;


import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;


public class MainActivity extends AppCompatActivity {


    public static final String GPS_FILTER = "GPSFilter";
    private ListView listView;
    private List<Plato> platos = new ArrayList<>();
    private SwipeRefreshLayout mSwipeRefreshLayout;
    private ProgressDialog loadingDialog;
    private GPSReceiver receiver;
    private TextView gpsText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        gpsText=(TextView)findViewById(R.id.gps_textview);
        listView=(ListView)findViewById(R.id.platos_listview);
        listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Intent i = new Intent(MainActivity.this,PedidosActivity.class);
                i.putExtra("name", ((Plato)platos.get(position)).getNombre());
                i.putExtra("id", ((Plato)platos.get(position)).getId());
                startActivity(i);
            }
        });

        mSwipeRefreshLayout = (SwipeRefreshLayout) findViewById(R.id.activity_main_swipe_refresh_layout);
        mSwipeRefreshLayout.setOnRefreshListener(new SwipeRefreshLayout.OnRefreshListener() {
            @Override
            public void onRefresh() {
                getPlatos();
            }
        });
        mSwipeRefreshLayout.setRefreshing(true);
        getPlatos();

        receiver = new GPSReceiver(gpsText);
        this.registerReceiver(receiver, new IntentFilter(GPS_FILTER));
        Intent intent = new Intent(this, GpsIntentService.class);
        intent.setAction(GpsIntentService.GETPOSITION);
        this.startService(intent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.main_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.alertTitle:
                AlertDialog dialog = createAlertDialog();
                dialog.show();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    public AlertDialog createAlertDialog(){
        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setMessage("Hello World")
                .setTitle("Dialogo de Alerta")
                .setPositiveButton("Ok", new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        //Do something
                    }
                });
        return builder.create();
    }

    public void getPlatos() {
        AsyncTask getPlatosWithDialog = new AsyncTask<Void,Void,Void>() {
            @Override
            protected void onPreExecute() {
                loadingDialog = new ProgressDialog(MainActivity.this);
                loadingDialog.setMessage("Cargando Platos");
                loadingDialog.setTitle("Espera...");
                loadingDialog.show();
            }
            @Override
            protected Void doInBackground(Void... params) {
                Call<List<Plato>> call = RestClient.getInstance().getApiService().getPlatos();
                try {
                    platos = call.execute().body();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                return null;
            }

            @Override
            protected void onPostExecute(Void aVoid) {
                PlatoAdapter itemsAdapter = new PlatoAdapter(platos,MainActivity.this);
                if(listView!=null) {
                    listView.setAdapter(itemsAdapter);
                }if(loadingDialog!=null){
                    loadingDialog.dismiss();
                }
                mSwipeRefreshLayout.setRefreshing(false);
            }
        }.execute();
    }


    private class GPSReceiver extends BroadcastReceiver {
        private TextView textView;
        private Exception exception;
        public GPSReceiver(TextView view){ textView = view; }
        public void onReceive(Context context, Intent intent) {
            String text = intent.getStringExtra("position");
            textView.setText(text);
        }
    }

}