package com.piazuelo.domiciliosmoviles.activities;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.provider.ContactsContract;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.ContextCompat;
import android.support.v4.content.CursorLoader;
import android.support.v4.content.Loader;
import android.support.v4.widget.SimpleCursorAdapter;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import com.piazuelo.domiciliosmoviles.R;

public class ContactsActivity extends AppCompatActivity implements LoaderManager.LoaderCallbacks<Cursor>, AdapterView.OnItemClickListener {

    public static final String[] PROJECTION =
            {
                    ContactsContract.Contacts._ID,
                    ContactsContract.Contacts.LOOKUP_KEY,
                    ContactsContract.Contacts.HAS_PHONE_NUMBER,
                    Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                            ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
                            ContactsContract.Contacts.DISPLAY_NAME
            };

    public static final String[] FROM_COLUMNS = {
            Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB ?
                    ContactsContract.Contacts.DISPLAY_NAME_PRIMARY :
                    ContactsContract.Contacts.DISPLAY_NAME
    };

    public static final int[] TO_IDS = {
            android.R.id.text1
    };

    public static final int LOADER_ID = 0;

    public static final int MY_PERMISSIONS_REQUEST_READ_CONTACTS = 69;

    // The column index for the _ID column
    private static final int CONTACT_ID_INDEX = 0;

    private SimpleCursorAdapter mCursorAdapter;
    private ListView contactslv;
    private String platoName;
    private String platoPrice;
    private int platoId;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_contacts);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);

        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        contactslv = (ListView) findViewById(R.id.contacts_listview);
        if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_CONTACTS) != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(
                    this,
                    new String[] { Manifest.permission.READ_CONTACTS },
                    MY_PERMISSIONS_REQUEST_READ_CONTACTS
            );
        } else {
            getSupportLoaderManager().initLoader(
                    LOADER_ID,  // The identifier of the loader to initialize
                    null,       // Arguments for the loader (in this case, none)
                    this
            );      // The context of the activity

            // Creates a new cursor adapter to attach to the list view
            mCursorAdapter = new SimpleCursorAdapter(
                    this,
                    R.layout.detail_contact,
                    null,
                    FROM_COLUMNS,
                    TO_IDS,
                    0
            );
            // Sets the ListView's backing adapter.
            contactslv.setAdapter(mCursorAdapter);
        }
        contactslv.setClickable(true);
        contactslv.setOnItemClickListener(this);
        if(getIntent() != null ) {
            if (getIntent().getExtras().getString("name") != null) {
                platoName = getIntent().getExtras().getString("name");
                getSupportActionBar().setTitle("Share " + platoName);
            }
            if (getIntent().getExtras().getString("price") != null) {
                platoPrice = getIntent().getExtras().getString("price");
            }
            if (getIntent().getExtras().getString("id") != null) {
                platoId = getIntent().getExtras().getInt("id");
            }
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_READ_CONTACTS: {
                // Si un request es cancelado el array resultado es vacío
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    // Ahora que el permiso fue otorgado se debe poner en este espacio lo relacionado con la solicitud de contactos.
                } else {
                }
                return;
            }
        }
    }

    @Override
    public Loader<Cursor> onCreateLoader(int loaderId, Bundle args) {
        // Starts the query
        return new CursorLoader(
                this,
                ContactsContract.Contacts.CONTENT_URI,
                PROJECTION,
                null,
                null,
                null
        );
    }

    @Override
    public void onLoadFinished(Loader<Cursor> loader, Cursor o) {
        mCursorAdapter.swapCursor(o);
    }

    @Override
    public void onLoaderReset(Loader<Cursor> loader) {
        mCursorAdapter.swapCursor(null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        // Get the Cursor
        Cursor cursor = ((SimpleCursorAdapter)parent.getAdapter()).getCursor();
        // Move to the selected contact
        cursor.moveToPosition(position);
        // Get the _ID value
        long mContactId = cursor.getLong(CONTACT_ID_INDEX);
        // Get the selected LOOKUP KEY
        String mContactKey = cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.DISPLAY_NAME));
        // Create the contact's content Uri
        Uri mContactUri = ContactsContract.Contacts.getLookupUri(mContactId, mContactKey);
        /*
         * You can use mContactUri as the content URI for retrieving
         * the details for a contact.
         */
        if (Integer.parseInt(cursor.getString(cursor.getColumnIndex(ContactsContract.Contacts.HAS_PHONE_NUMBER))) > 0)
        {
            // Query phone here. Covered next
            Cursor phones = getContentResolver()
                    .query(ContactsContract.CommonDataKinds.Phone.CONTENT_URI,
                            null,
                            ContactsContract.CommonDataKinds.Phone.CONTACT_ID +" = "+ id,
                            null,
                            null
                    );
            String onePhone = "";
            while (phones.moveToNext()) {
                onePhone = phones.getString(phones.getColumnIndex(ContactsContract.CommonDataKinds.Phone.NUMBER));
                Log.i("Number", onePhone);
            }
            //Send intent
            Intent intent = new Intent(Intent.ACTION_SENDTO);
            intent.setData(Uri.parse("smsto:"+onePhone));

            intent.putExtra("sms_body", platoName + " - " + platoPrice);
            if (intent.resolveActivity(getPackageManager()) != null) {
                startActivity(intent);
            }
            phones.close();
        } else {
            Log.d("Tag","no number");
        }
    }
}
