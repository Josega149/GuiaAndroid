package com.piazuelo.domiciliosmoviles.entities;

/**
 * Created by Nicolás on 11/09/17.
 */

public class Pedido {
    private int idPlato;
    private String cliente;
    private String lugar;

    public Pedido(int idPlato, String cliente, String lugar) {
        this.idPlato = idPlato;
        this.cliente = cliente;
        this.lugar = lugar;
    }

    public int getIdPlato() {
        return idPlato;
    }

    public String getCliente() {
        return cliente;
    }

    public String getLugar() {
        return lugar;
    }
}
