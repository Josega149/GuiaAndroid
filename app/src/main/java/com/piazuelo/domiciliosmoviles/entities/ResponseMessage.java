package com.piazuelo.domiciliosmoviles.entities;

/**
 * Created by jgtamura on 10/17/17.
 */

public class ResponseMessage {

    private String msg;

    public ResponseMessage(String msg){
        this.msg = msg;
    }

    public String getMsg() {
        return msg;
    }
}